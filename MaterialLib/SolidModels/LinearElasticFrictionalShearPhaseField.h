/**
 * \file
 * \copyright
 * Copyright (c) 2012-2025, OpenGeoSys Community (http://www.opengeosys.org)
 *            Distributed under a Modified BSD License.
 *              See accompanying file LICENSE.txt or
 *              http://www.opengeosys.org/project/license
 */

#pragma once

#include <Eigen/Eigenvalues>
#include <boost/math/constants/constants.hpp>
#include <cmath>
#include <algorithm>
#include "MathLib/KelvinVector.h"

namespace MaterialLib
{
namespace Solids
{
namespace Phasefield
{

template <int DisplacementDim>
std::tuple<MathLib::KelvinVector::KelvinVectorType<DisplacementDim>,  // sigma_real
           MathLib::KelvinVector::KelvinVectorType<DisplacementDim>,  // sigma_tensile
           MathLib::KelvinVector::KelvinMatrixType<DisplacementDim>,  // D
           double,  // strain_energy_tensile
           double,  // elastic_energy
           double,  // history_slip
           double,  // history_threshold
           MathLib::KelvinVector::KelvinMatrixType<DisplacementDim>,  // C_tensile
           MathLib::KelvinVector::KelvinMatrixType<DisplacementDim>>  // C_compressive
calculateFrictionalShearDegradedStress(
    double const degradation,
    double const bulk_modulus,
    double const mu,
    double const friction_angle,
    double const cohesion,
    double const friction_angle_residual,
    double const cohesion_residual,
    MathLib::KelvinVector::KelvinVectorType<DisplacementDim> const& eps)
{
    using KelvinVector = MathLib::KelvinVector::KelvinVectorType<DisplacementDim>;
    using KelvinMatrix = MathLib::KelvinVector::KelvinMatrixType<DisplacementDim>;
    using Invariants = MathLib::KelvinVector::Invariants<
        MathLib::KelvinVector::kelvin_vector_dimensions(DisplacementDim)>;

    // Convert angles to radians
    double const pi = boost::math::constants::pi<double>();
    double const friction_angle_rad = friction_angle * pi / 180.0;
    double const friction_angle_residual_rad = friction_angle_residual * pi / 180.0;

    // Calculate volumetric strain
    double const eps_vol = Invariants::trace(eps);
    
    // Calculate deviatoric strain and equivalent strain
    auto const& P_dev = Invariants::deviatoric_projection;
    KelvinVector const eps_dev = P_dev * eps;
    double const eps_eq = std::sqrt(2.0/3.0 * eps_dev.dot(eps_dev));

    // Calculate equivalent stress from strain (σ̂_eq)
    double const sigma_eq_hat = 3.0 * mu * eps_eq;

    // Calculate initial stress state
    double const lambda = bulk_modulus - (2.0/3.0) * mu;
    KelvinVector const sigma0 = 2.0 * mu * eps + lambda * eps_vol * Invariants::identity2;
    
    // Calculate mean stress considering plane strain
    double const nu = (3*bulk_modulus - 2*mu)/(2*(3*bulk_modulus + mu));
    double const sigma_xx = sigma0[0];
    double const sigma_yy = sigma0[1];
    double const sigma_zz = (nu / (1 - nu)) * (sigma_xx + sigma_yy);
    double const p = (sigma_xx + sigma_yy + sigma_zz) / 3.0;

    // Calculate deviatoric stress and von Mises stress
    KelvinVector const dev_stress = P_dev * sigma0;
    double const q = std::sqrt(1.5 * dev_stress.dot(dev_stress));

    // Calculate unit deviatoric tensor from strain
    KelvinVector alpha_eq = KelvinVector::Zero();
    if (eps_eq > 1e-15)
    {
        alpha_eq = std::sqrt(2.0/3.0) * eps_dev / eps_eq;
    }

    // Calculate third invariant and Lode angle from stress
    auto stress_tensor = MathLib::KelvinVector::kelvinVectorToTensor(dev_stress);
    double const third_invariant = stress_tensor.determinant();
    double const r3 = std::pow(3.0, 3) * (0.5 * third_invariant);
    double const cos_three_theta = r3 / std::pow(q, 3);
    double const theta = std::acos(std::clamp(cos_three_theta, -1.0, 1.0)) / 3.0;

    // Calculate R_MC 
    double const R_MC = (1.0 / (std::sqrt(3.0) * std::cos(friction_angle_rad))) * 
                       std::sin(theta + pi / 3.0) + 
                       (1.0/3.0) * std::tan(friction_angle_rad) * 
                       std::cos(theta + pi / 3.0);

    // Calculate characteristic stresses
    double const sigma_eq_p = (p * std::tan(friction_angle_rad) + cohesion) / R_MC;
    double const sigma_eq_r = (p * std::tan(friction_angle_residual_rad) + cohesion_residual) / R_MC;

    // Calculate history variables 
    double const history_threshold = std::pow(sigma_eq_p - sigma_eq_r, 2) / (6.0 * mu);
    double const history_slip = (std::pow(sigma_eq_hat - sigma_eq_r, 2) - 
                                std::pow(sigma_eq_p - sigma_eq_r, 2)) / (6.0 * mu);

    // Calculate real stress based on equation (15)
    KelvinVector const sigma_real = 3.0 * bulk_modulus * eps_vol * Invariants::identity2 + 
                                  (degradation * sigma_eq_hat + (1.0 - degradation) * sigma_eq_r) * 
                                  std::sqrt(2.0/3.0) * alpha_eq;

    // Calculate stiffness matrix D using Haghighat approach
    KelvinMatrix D = KelvinMatrix::Zero();

    // Lambda parameter
    double const lambda_param = bulk_modulus - (2.0/3.0) * mu;

    // Fill normal components - diagonal terms
    for (int i = 0; i < (DisplacementDim == 3 ? 3 : 2); ++i)
    {
        D(i, i) = 2.0 * mu + lambda_param;
    }

    // Fill normal components - off-diagonal terms (volumetric coupling)
    for (int i = 0; i < (DisplacementDim == 3 ? 3 : 2); ++i)
    {
        for (int j = 0; j < (DisplacementDim == 3 ? 3 : 2); ++j)
        {
            if (i != j)
            {
                D(i, j) = lambda_param;
            }
        }
    }

    // For 3D case
    if (DisplacementDim == 3)
    {
        // Shear components with degradation applied only to xy component
        D(3, 3) = 2.0 * mu * degradation;  // xy component
        D(4, 4) = 2.0 * mu;                // yz component (no degradation)
        D(5, 5) = 2.0 * mu;                // xz component (no degradation)
    }
    else // 2D case
    {
        // Only xy shear component with degradation
        D(2, 2) = 2.0 * mu * degradation;  // xy component
    }

    // Calculate tensile stress
    KelvinVector const sigma_tensile = 3.0 * bulk_modulus * eps_vol * Invariants::identity2 + 
                                     sigma_eq_hat * std::sqrt(2.0/3.0) * alpha_eq;

    // Calculate strain energies
    double const strain_energy_tensile = 
        (3.0 * bulk_modulus * eps_vol * eps_vol / 2.0) + (sigma_eq_hat * sigma_eq_hat / (6.0 * mu));
    double const elastic_energy = degradation * strain_energy_tensile;

    // Return tensile and compressive stiffness matrices for output
    KelvinMatrix C_tensile = KelvinMatrix::Zero();
    KelvinMatrix C_compressive = KelvinMatrix::Zero();

    return std::make_tuple(sigma_real, sigma_tensile, D, strain_energy_tensile,
                          elastic_energy, history_slip, history_threshold,
                          C_tensile, C_compressive);
}

}  // namespace Phasefield
}  // namespace Solids
}  // namespace MaterialLib